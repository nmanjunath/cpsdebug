function cpsdebug(cpsdebug_config, result_path)
% FUNCTION: cpsdebug
% Created: 07-11-2019
% Author:  Niveditha Manjunath

% profile on

%% Add required folders to path
addpath( 'Configuration', 'Source', 'ModelOperations', 'Model', 'TestSuite' );

%% System Diagnostics
system_diagnostics( cd, ['Configuration/', cpsdebug_config], result_path );

% %% Save profiler data
% profile off
% profsave(profile('info'), [result_path, '/myprofile_results']);

end