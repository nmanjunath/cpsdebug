function [ sim_time, combined_simout, signal_names, combined_nb_test ] = test_suite_aircraft( model_copy_name_wo_ext, constants_thresholds, names_table, system_handle, LUTVar )
% FUNCTION: The test suite should generate tests for the model, perform
%       fault injections, simulate the model and record the data in the
%       required format to be presented to the next phases of the tool
% Created:  13-11-2019
% Author:   Niveditha Manjunath
% Note:     This section is user and model specific, the tool concerns only
%       with the output of this funtion

load(constants_thresholds); % Parameters necessary to perform simulations of the system (user defined as a .mat file)

disp(['test_suite_aircraft.m: Simulating ', model_copy_name_wo_ext, '...' ]);

%% Model simulator and data accumulator

set_param(system_handle, 'Solver', 'ode4', 'StopTime', num2str(stop_time), ...
    'ReturnWorkspaceOutputs', 'on');
[ sim_time, simout, signal_names, nb_test ] = test_sim_aircraft( model_copy_name_wo_ext, names_table, ...
    samp_freq, in_freq_begin, in_freq_end, freq_step, in_amp_begin, in_amp_end, ...
    amp_step, start_time, stop_time );

%% Fault injection, simulation, diagostics and analysis

[ test_fault_list ] = gen_fault_list( nb_injected_faults );

for i = 1:length(test_fault_list)
    
    fault_list = {'reset'}; %{'LI', 'LO', 'RO', 'RI', 'H1', 'H2', 'H3', 'reset'}; Use 'reset' as default
    
    [ fault_injection ] = inject_fault( model_copy_name_wo_ext, fault_list );
    
    [ failed_sim_time, failedSimout, failed_signal_names, failed_nb_test ] = test_sim_aircraft( model_copy_name_wo_ext, names_table, ...
        faulty_samp_freq, faulty_in_freq_begin, faulty_in_freq_end, faulty_freq_step, ...
        faulty_in_amp_begin, faulty_in_amp_end, faulty_amp_step, start_time, stop_time ); % Fault-injected simulations
    
    fault_list = test_fault_list{i};
    
    [ fault_injection ] = inject_fault( model_copy_name_wo_ext, fault_list );
    
    if (fault_injection == 0)
        disp(['test_suite_aircraft.m: Fault injection of ', fault_list, ' was not succesful.']);
    else
        disp(['test_suite_aircraft.m: Fault injection of ', fault_list, ' was succesful.']);
    end
    
    set_param(system_handle, 'Solver', 'ode4', 'StopTime', num2str(stop_time), ...
        'ReturnWorkspaceOutputs', 'on');
    
    disp(['test_suite_aircraft.m: Simulating ', model_copy_name_wo_ext, ' with injected fault: ', fault_list, '...' ]);
    
    [ failed_sim_time, failedSimout, failed_signal_names, failed_nb_test ] = test_sim_aircraft( model_copy_name_wo_ext, names_table, ...
        faulty_samp_freq, faulty_in_freq_begin, faulty_in_freq_end, faulty_freq_step, ...
        faulty_in_amp_begin, faulty_in_amp_end, faulty_amp_step, start_time, stop_time ); % Fault-injected simulations
    
    failed_simout{i} = cell2mat(failedSimout);
    
end

% %% Data from Constrained-Optimization
% 
% fault_list = {'reset'}; %{'LI', 'LO', 'RO', 'RI', 'H1', 'H2', 'H3', 'reset'}; Use 'reset' as default
%     
% [ fault_injection ] = inject_fault( model_copy_name_wo_ext, fault_list );
%     
% [ co_sim_time, co_simout, co_signal_names, co_nb_test ] = test_sim_aircraft( model_copy_name_wo_ext, names_table, ...
%         samp_freq, 0.1, 0.1, faulty_freq_step, ...
%         0.026405, 0.026405, faulty_amp_step, start_time, stop_time ); % Fault-injected simulations

%% Combining simulated data
% combined_simout = horzcat(simout, failed_simout, co_simout);
combined_simout = horzcat(simout, failed_simout);
[m, n] = size(combined_simout{1});
if (m ~= length(sim_time) && sim_time(1) > 0)
    sim_time = vertcat(0, sim_time);
end

save_system(system_handle);
close_system(system_handle);

%% Export total number of simulated tests
combined_nb_test = length(combined_simout)

%% Plotting figures
figure_plots(combined_simout);

end