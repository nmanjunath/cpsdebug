function [ test_fault_list ] = gen_fault_list( nb_faults )
% FUNCTION: Generate CSV files of simulated data (for AMT)
% Created:  26-02-2019
% Author:   Niveditha Manjunath

test_fault_list{1} = { 'H1', 'H2' };
test_fault_list{2} = { 'H1', 'H3' };
test_fault_list{3} = { 'H1', 'RO' };
test_fault_list{4} = { 'H1', 'RI' };
test_fault_list{5} = { 'H1', 'LO' };
test_fault_list{6} = { 'H1', 'LI' };
test_fault_list{7} = { 'H2', 'H3' };
test_fault_list{8} = { 'H2', 'RO' };
test_fault_list{9} = { 'H2', 'RI' };
test_fault_list{10} = { 'H2', 'LO' };
test_fault_list{11} = { 'H2', 'LI' };
test_fault_list{12} = { 'H3', 'RO' };
test_fault_list{13} = { 'H3', 'RI' };
test_fault_list{14} = { 'H3', 'LO' };
test_fault_list{15} = { 'H3', 'LI' };
test_fault_list{16} = { 'RO', 'RI' };
test_fault_list{17} = { 'RO', 'LO' };
test_fault_list{18} = { 'RO', 'LI' };
test_fault_list{19} = { 'RI', 'LO' };
test_fault_list{20} = { 'RI', 'LI' };
test_fault_list{21} = { 'LO', 'LI' };

disp('gen_fault_list.m: Fault list for injection generated');

end