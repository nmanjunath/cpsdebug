function [ sim_time, simout, signal_names, nb_test ] = test_sim_autotrans( model_copy_name_wo_ext, names_table, samp_freq, throttle_begin, throttle_change_time, throttle_end, start_time, stop_time, nb_throttle_begin, nb_throttle_change_time, nb_throttle_end )
% FUNCTION: Simulate tests with generated inputs
% Created:  05-10-2018
% Author:   Niveditha Manjunath
% Note:     Export simulation time and external observables
% Hardcoded: Gear signal and  Transition signal
% {s45-{'autotrans_mod04_copy/ShiftLogic:1-->autotrans_mod04_copy/gear:1'}} has been explicitly denoted enumerated in the variable 'signal names', usually this is of the form integer
% {s32-{'autotrans_mod04_copy/ShiftLogic:2-->autotrans_mod04_copy/transitionlog:1'}}

nb_test = 0;
dt = samp_freq; % Sampling frequency (Compute automatically in future)
time_var = (start_time:dt:stop_time)';
signal_length = length(time_var);
break_input = zeros(size(time_var));
simopt = simget(model_copy_name_wo_ext);
simopt = simset(simopt,'SaveFormat','Array', ...
    'SignalLogging', 'on', 'SignalLoggingName', 'logsout');

for i = 1:nb_throttle_begin
    for j = 1:nb_throttle_change_time
        for k = 1:nb_throttle_end
            nb_test = nb_test + 1;
            disp(strcat('test_sim_autotrans.m: Simulating Test_', num2str(nb_test)));
            range_var = [(i*throttle_begin),((i*throttle_begin)+(k*throttle_end))];
            change_time = find(time_var==(j*throttle_change_time));
            throttle = (repelem(range_var,[change_time,(length(time_var)-change_time)]))';
            Input = [time_var,throttle,break_input];
            simOut = sim(model_copy_name_wo_ext,[Input(1,1,1) Input(end,1,1)],simopt, Input);
            data{nb_test} = simOut.get('logsout');
            signalNames = getElementNames(data{nb_test}); % The Name should be assigned accordingly since a signal can have multiple data values in timeseries
            index = 1;
            for k = 1:length(getElementNames(data{nb_test}))
            logsoutElem(k) = get(data{nb_test},k);
            zero_add_above = 0;
            zero_add_below = 0;
            %logsoutName = logsoutElem(k).Values.Name; % Not all Values may have Names for Data
            if (isprop(logsoutElem(k).Values,'Data'))
                logsoutData_woi = logsoutElem(k).Values.Data;
                [m_logsoutData_woi,n_logsoutData_woi] = size(logsoutData_woi);
                if (n_logsoutData_woi == 1)
                    if (m_logsoutData_woi < signal_length)
                        if (logsoutElem(k).Values.Time(1) ~= start_time) % Append zeros to the signal in the beginning or end or both as necessary
                            zero_add_above = uint64(logsoutElem(k).Values.Time(1)/dt);
                            logsoutData_fi = [];
                            logsoutData_fi(zero_add_above,1) = 0;
                            logsoutData_woi = cat(1,logsoutData_fi,logsoutData_woi);
                        end
                        if (logsoutElem(k).Values.Time(end) ~= stop_time)
                            zero_add_below = signal_length - uint64(logsoutElem(k).Values.Time(end)/dt) - zero_add_above;                            
                            logsoutData_fi = [];
                            logsoutData_fi(zero_add_below,1) = 0;
                            logsoutData_woi = cat(1,logsoutData_woi,logsoutData_fi);
                        end
                        logsoutData_array(:,index) = double(logsoutData_woi);
                    else
                        logsoutData_array(:,index) = double(logsoutData_woi);
                    end
                    logsoutData_final{index} = logsoutData_woi;
                    signal_names{index,1} = signalNames{k};
                    index = index + 1;
                end
            else
                logsoutData_struct = logsoutElem(k).Values;
                logsoutData_cell = struct2cell(logsoutData_struct);
                [m_logsoutData_cell,n_logsoutData_cell] = size(logsoutData_cell);
                for l = 1:m_logsoutData_cell
                    logsoutData_w = logsoutData_cell{l}.Data;
                    logsoutData_wi = logsoutData_cell{l}.Data;
                    [m_logsoutData_wi,n_logsoutData_wi] = size(logsoutData_wi);
                    if (m_logsoutData_wi < signal_length)
                        if (logsoutData_cell{1}.Time(1) ~= start_time) % Append zeros to the signal in the beginning or end or both as necessary
                            zero_add_above = uint64(logsoutData_cell{1}.Time(1)/dt);
                            logsoutData_f = [];
                            logsoutData_f(zero_add_above,n_logsoutData_wi) = 0;
                            logsoutData_w = cat(1,logsoutData_f,logsoutData_wi);
                        end
                        if (logsoutData_cell{1}.Time(end) ~= stop_time)
                            zero_add_below = signal_length - uint64(logsoutData_cell{1}.Time(end)/dt) - zero_add_above;
                            logsoutData_f = [];
                            logsoutData_f(zero_add_below,n_logsoutData_wi) = 0;
                            logsoutData_w = cat(1,logsoutData_wi,logsoutData_f);
                        end
                    end
                    if (n_logsoutData_wi == 1)
                        logsoutData_array(:,index) = double(logsoutData_w);
                        logsoutData_final{index} = logsoutData_w;
                        signal_names{index,1} = signalNames{k};
                        index = index + 1;
                    end
                end
            end
        end
        simout{nb_test} = logsoutData_array;
        end
    end
end

sim_time = simOut.get('tout');

% Until now, the variable signal_names only contained the custom names
% that were created. For simplification of computation, we add 
% BlockIndex (group number) from names table to the 2nd column
for m = 1:length(signal_names)
    idx = find(strcmp(names_table.ShortName, signal_names{m,1}));
    signal_names{m,2} = cell2mat(names_table.BlockIndex(idx));    
    signal_names{m,3} = cell2mat(names_table.SignalType(idx));
end

% Make gear (s45) and transitionlog (s32) as enumerated signals

signal_names{10,3} = 1;
signal_names{11,3} = 1;

end