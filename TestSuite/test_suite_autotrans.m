function [ sim_time, simout, signal_names, nb_test ] = test_suite_autotrans( model_copy_name_wo_ext, constants_thresholds, names_table, system_handle, LUTVar )
% FUNCTION: The test suite should generate tests for the model, perform
%       fault injections, simulate the model and record the data in the
%       required format to be presented to the next phases of the tool
% Created:  13-11-2019
% Author:   Niveditha Manjunath
% Note:     This section is user and model specific, the tool concerns only
%       with the output of this funtion

load(constants_thresholds); % Parameters necessary to perform simulations of the system (user defined as a .mat file)

disp(['test_suite_autotrans.m: Simulating ', model_copy_name_wo_ext, '...' ]);

%% Model simulator and data accumulator

set_param(system_handle, 'Solver', 'ode4', 'StopTime', num2str(stop_time), ...
    'ReturnWorkspaceOutputs', 'on');
	
[ sim_time, simout, signal_names, nb_test ] = test_sim_autotrans( model_copy_name_wo_ext, names_table, samp_freq, throttle_begin, throttle_change_time, throttle_end, start_time, stop_time, nb_throttle_begin, nb_throttle_change_time, nb_throttle_end );

save_system(system_handle);
close_system(system_handle);

%% Generating and adding LUT index signals

disp(['test_suite_autotrans.m: Generating ', model_copy_name_wo_ext, ' LUT signals...' ]);

[nb_samples, nb_signals] = size(simout{1});
names_table_height = height(names_table);
[m, n] = size(LUTVar.InSignalNameVar);
for i = 1:m
    var_index = 1;
    for j = 1:n 
        OutSignalIndex(var_index) = find(ismember(signal_names(:,1), LUTVar.InSignalNameVar(i,j)));
        var_index = var_index + 1;
    end
    if (~isempty(regexp(cell2mat(LUTVar.RowIndexVar(i)),':')))
        RowVector = str2num(cell2mat(LUTVar.RowIndexVar(i)));
    else
        RowVector = eval(cell2mat(LUTVar.RowIndexVar(i)));
    end
    if (~isempty(regexp(cell2mat(LUTVar.ColumnIndexVar(i)),':')))
        ColumnVector = str2num(cell2mat(LUTVar.ColumnIndexVar(i)));
    else
        ColumnVector = eval(cell2mat(LUTVar.ColumnIndexVar(i)));
    end
    
    LUT_index_signal = [];
    for j = 1:length(simout)
        for k = 1:nb_samples
            %Index = x+n*y; where x is the column index and y is the row index
            ColumnVar = (find(ColumnVector <= simout{j}(k,OutSignalIndex(2)),1,'last'))-1;
            if ( isempty(ColumnVar) )
                ColumnVar = 0;
            end
            RowVar = (find(RowVector <= simout{j}(k,OutSignalIndex(1)),1,'last'))-1;
            if ( isempty(RowVar) )
                RowVar = 0;
            end
            LUT_index_signal(k) = ColumnVar+(length(ColumnVar)*RowVar);
        end
        simout{j}(:,nb_signals + i) = LUT_index_signal;
    end
    signal_names{nb_signals + i, 1} = names_table.ShortName{names_table_height - m + i};
    signal_names{nb_signals + i, 2} = names_table.BlockIndex{names_table_height - m + i};
    signal_names{nb_signals + i, 3} = names_table.SignalType{names_table_height - m + i};
end

% [ simout ] = add_LUT_index( simout, signal_names, names_table, LUTVar ); % This function is typically used in auto_trans test suite

end