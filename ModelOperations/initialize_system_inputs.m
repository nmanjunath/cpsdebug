function [ model, stl_spec_file, test_suite, constants_thresholds ] = initialize_system_inputs( file_name )
% FUNCTION: Extracts file names inputs to system diagnostics
%           (Names of: Model, STL Spec file, Test suite, File name and prefix, Constants and thresholds)
% Created:  11-11-2019
% Author:   Niveditha Manjunath

function_inputs = readtable( file_name, 'Delimiter', ',' );

% Function input files and parameters
model = ['Model/', cell2mat(function_inputs.model)];
stl_spec_file = ['Configuration/', cell2mat(function_inputs.stl_spec_file)];
test_suite = cell2mat(function_inputs.test_suite);
constants_thresholds = ['Configuration/', cell2mat(function_inputs.constants_thresholds)];

disp('initialize_system_inputs.m: Initialized system inputs successfully');

end