function [ updated_invariants ] = RTAMT_rel_op_patch( invariants )
% FUNCTION: Takes input Daikon generated properties and adapts the relational operators part of the formula for RTAMT
% Created:  21-01-2020
% Author:   Niveditha Manjunath

operator_pos = regexp(invariants,'(==)|(!==)|(<)|(>)|(<=)|(>=)');
signal_pos = regexp(invariants,'s[0-9]+');
or_pos = regexp(invariants,'(one of)|(has_values)'); % Check if the assertion is oy type always((s*==x)or(s*==y)or(s*==z))
space_pos = regexp(invariants,' ');

for i = 1:length(invariants)
    if (isempty(cell2mat(or_pos(1))))
        if (length(signal_pos{i}) > 1)
                end_pos_sig = space_pos{i}(find(space_pos{i} > signal_pos{i}(1), 1));
                end_pos_op = space_pos{i}(find(space_pos{i} > operator_pos{i}(1), 1));
                updated_invariants{i,1} = ['( ', invariants{i}(signal_pos{i}(1):end_pos_sig-1), ' - ', invariants{i}(signal_pos{i}(2):end), ' ) ', invariants{i}(operator_pos{i}(1):end_pos_op) ' 0'];
        else
            updated_invariants{i,1} = invariants{i};
        end
    else
        updated_invariants{i,1} = invariants{i};
    end
end

end