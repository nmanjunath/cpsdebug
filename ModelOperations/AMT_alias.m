function [ aliasfilename ] = AMT_alias( result_path, header_var, alias_file_name )
% FUNCTION: Generate .alias input file for AMT
% Created:  28-10-2018
% Author:   Niveditha Manjunath

% AMT requires several input files to perform monitoring and diagnostics
% 1. Test input as .csv file
% 2. formula.alias
% 3. formula.stl


% Since MATLAB cannot generate files with .alias and .stl extensions, we
% generate .txt files for the same and rename extensions later

% Generate content of formula.alias file as a text file
% and rename as formula.alias

for i = 1:length(header_var)
    alias_var{i,1} = [header_var{1,i}, ' = ', header_var{1,i}];
end
alias = table(alias_var);
writetable(alias, 'alias.txt', 'WriteVariableNames', false);
aliasfilename = [result_path, '\' alias_file_name];
movefile('alias.txt', aliasfilename);


end