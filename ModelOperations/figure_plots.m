function figure_plots(simout)
% FUNCTION: Plotting figures
% Created:  24-01-2019
% Author:   Niveditha Manjunath

plotCommandSignal = simout{1}(:,63);% Index=63
time_var = [0:0.01:10]';
leftElevatorPositionSignal = simout{1}(:,188);% Index=188
figure(1);
plot(time_var,plotCommandSignal, '--b', time_var, leftElevatorPositionSignal, 'r', 'LineWidth', 2);
xlabel('Time (s)');
ylabel('Relative Position');
set(gcf,'Color','w');
legend('Pilot Command', 'Left Elevator Position');
saveas(gcf, 'Results/PassTest.fig');

% Plotting observable signals from 1 failing test 
plotCommandSignal = simout{171}(:,63);% Index=63
time_var = [0:0.01:10]';
leftElevatorPositionSignal = simout{171}(:,188);% Index=188
figure(2);
plot(time_var,plotCommandSignal, '--b', time_var, leftElevatorPositionSignal, 'r', 'LineWidth', 2);
xlabel('Time (s)');
ylabel('Relative Position');
set(gcf,'Color','w');
legend('Pilot Command', 'Left Elevator Position');
saveas(gcf, 'Results/FailTest.fig');