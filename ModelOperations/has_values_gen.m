function [ assertion ] = has_values_gen( has_values_invariant )
% FUNCTION: Generate 'has values:' type assertions
% Created:  02-10-2019
% Author:   Niveditha Manjunath


sig_name = has_values_invariant(1:regexp(has_values_invariant,' has values:'));
invariant = regexprep(has_values_invariant,'s[0-9]+ has values:','');
invariant = erase(invariant,' ');
val_list = str2num(regexprep(invariant,'\[[0-9]+\]',','));
gen_assertion = [];
assertion = [];
for i = 1:length(val_list)
        gen_assertion = [ '( ', sig_name, '== ', num2str(val_list(i)), ' )' ];
        if (i > 1)
            assertion = [assertion, ' or ', gen_assertion ];
        else
            assertion = gen_assertion;
        end
end


end
