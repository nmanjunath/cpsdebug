function [ assertions_signals, assertions_signals_index, assertions_signal_type ] = assertion_signal_list( assertions, signal_names )
% FUNCTION: Generate a list of signals present in the assertions
% Created:  09-01-2020
% Author:   Niveditha Manjunath

% Find the signals and indices of signal names present in the assertion list
signalname_beginpos = regexp(assertions,'s[0-9]+');
signalname_endpos = regexp(assertions,' ');
assertions_signals = [];
assertions_signals_index = [];
for i = 1:length(signalname_beginpos)
    signal_list = [];
    signal_index = [];
    for j = 1:length(signalname_beginpos{i})
        ind{i,1}(j,1) = find(signalname_endpos{i} > signalname_beginpos{i}(j),1);
        signal_list{j} = assertions{i}(signalname_beginpos{i}(j):signalname_endpos{i}(ind{i,1}(j,1))-1);
        signal_index{j} = find(ismember(signal_names(:,1), signal_list{j}));
    end
    assertions_signals{i} = unique(signal_list, 'stable');
    assertions_signals_index{i} = unique(cell2mat(signal_index), 'stable');
end

for i = 1:length(assertions_signals_index)
    if (length(assertions_signals_index{i}) > 0)
        for j = 1:length(assertions_signals_index{i})
            signal_type{i}(j) = signal_names(assertions_signals_index{i}(j),3);
            if (cell2mat(signal_type{i}(j)) == 0)
                type_trans{j} = 'float';
            else if (cell2mat(signal_type{i}(j)) == 1 || cell2mat(signal_type{i}(j)) == 2 || cell2mat(signal_type{i}(j)) == 3)
                    type_trans{j} = 'int';
                else
                    type_trans{j} = 'float';
                end                
            end
        end
        assertions_signal_type{i} = type_trans;
    end
end

end