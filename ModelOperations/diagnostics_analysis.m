function [ flag ] = diagnostics_analysis( result_path, faulty_nb_test, nb_clusters )
% FUNCTION: Analyze violations resulted from AMT monitors
% Created:  23-11-2018
% Author:   Niveditha Manjunath

monitor_file_prefix = [result_path, '/Monitor_'];
diagnostics_files = dir(fullfile([pwd, '/', result_path], 'Monitor_*'));


% First we read all violated epochs from the diagnostics file

for i = 1:length(diagnostics_files)
    filename = diagnostics_files(i).name;
    violations = table2cell(readtable(filename,'Delimiter',',','ReadVariableNames',false,'HeaderLines',0));
    index = 1;
    for j = 1:length(violations)
        if (j > 1)
            Epoch_val1 = str2num(violations{j,2});
            Epoch_val2 = str2num(violations{j,3});
        else
            Epoch_val1 = zeros;
            Epoch_val2 = zeros;
        end
        if (isempty(Epoch_val1))
            Epoch_val1 = zeros;
        end
        if (isempty(Epoch_val2))
            Epoch_val2 = zeros;
        end
        Epoch(j,1) = Epoch_val1;
        Epoch(j,2) = Epoch_val2;
        if (regexp(violations{j,1},'^[%]') == 1)
            property_index(index) = j;
            property{index,1} = violations{j,1};
            index = index + 1;
        end
        property_index(index) = j;
    end
    
% We obtain the first occurences of the failed assertions
    for k = 1:index-1
        failed_assertions{k,1} = violations{property_index(k),1};
        failed_assertions{k,2} = min(min(Epoch(property_index(k)+1:property_index(k+1)-1,1:2)));
%         assertions(k,1) = min(min(Epoch(property_index(k)+1:property_index(k+1)-1,1:2)));
        assertions(k,1) = failed_assertions{k,2};
    end
end


% Algorithm for k-means clustering
% We perform k-means clustering to cluster the occurrences of the violated
% assertions. The maximum number of iterations are manually chosen by the
% user. Iteratively, we increase the number of clusters to obtain the
% optimal number of clusters required for our assertions. This optimal
% number of clusters are obtained by observing the sum of the squared error
% (sumd)

accumulated_error(1:nb_clusters) = zeros;
for l = 1:nb_clusters
    [idx,C,sumd,D] = kmeans(assertions,l);
    idx_arr(:,l) = idx;
    C_arr{l,1} = C;
    sumd_arr{l,1} = sumd;
    D_arr{l,1} = D;
    accumulator = zeros;
    for m = 1:length(sumd)
        accumulator = accumulator + sumd(m);
    end
    accumulated_error(l) = accumulator;
end

norm_accumulated_error = (abs(accumulated_error)./max(abs(accumulated_error)))';
diff_error = diff(norm_accumulated_error);

% Plot Elbow Point figure
figure(4);
plot_handle = plot((1:1:nb_clusters), accumulated_error, '-o', 'Color', 'b', 'LineWidth', 2);
% title('Elbow Point');
xlabel('No. of clusters');
ylabel('Sum of the squared error');
set(gcf,'Color','w');
saveas(gcf, 'CAVPlots/ElbowPoint.fig');
saveas(gcf, 'CAVPlots/ElbowPoint.pdf');
saveas(gcf, 'CAVPlots/ElbowPoint.png');

% Generate .csv file with a list of assertions and the first time each
% assertion was violated
assertion_file_name = 'CAVPlots/faulty_assertions.csv';
writetable(cell2table(failed_assertions), assertion_file_name, 'Delimiter', ',', 'WriteVariableNames', false);

flag = 1;

end