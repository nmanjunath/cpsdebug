function [ groupMap, flag ] = signals_sel( signal_names, keep_sig )
% FUNCTION: Write signals in Daikon input format
% Created:  17-10-2018
% Author:   Niveditha Manjunath

% % Create Hash map of signals to be written
% groupMap = containers.Map;
% group_index = 1; %Initialize a default value
% 
% for i = 1:length(signal_names)
%     if (keep_sig(i))
%         if (isKey(groupMap, group_index))
%             signal_names{i,1} = values(groupMap, group_index);
%         else
%             group_index = signal_names{i,2};
%             groupMap(signal_names{i,1}) = group_index;
%         end
%     end
% end

% for i = 1:length(signal_names)
%     if (keep_sig(i))
%         if (isKey(groupMap, group_index))
%             group_index = values(groupMap, signal_names{i,1});
%         else
%             group_index = signal_names{i,2};
%             groupMap(signal_names{i,1}) = group_index;
%         end
%     end
% end

flag = 1;

end