function file_prefix_initialization( file_name )
% FUNCTION: Reads variables to be initialized from file and assigns to base workspace
% Created:  11-11-2019
% Author:   Niveditha Manjunath

assign_vars = readtable( file_name, 'ReadVariableNames', false, 'Delimiter', ' ' );
for i = 1:height(assign_vars)
    evalin( 'caller', [cell2mat( assign_vars.Var1(i)), cell2mat( assign_vars.Var2(i)), cell2mat( assign_vars.Var3(i))] );
end

end