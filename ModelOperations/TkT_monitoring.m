function TkT_monitoring( top_directory, result_path_prefix, simout, enumerated_signal_names, enumerated_signal_index )
% FUNCTION: Mining timed automata and monitoring - Using TkT
% Created:  11-11-2019
% Author:   Niveditha Manjunath

% for i = 1:length(simout)
    for j = 1:length(enumerated_signal_index)
%         if ( keep_sig(enumerated_signal_index(j)) == 1 )
        cd ([top_directory, '/', result_path_prefix, '/TkT/', cell2mat(enumerated_signal_names(j))]);
        infer_command = ['java -Dtkt.policy.normalDistributionConfidence=0.99 -cp tkt.jar it.unimib.disco.lta.timedKTail.ui.InferModel TA.jtml validTraces/ > InferModel_', cell2mat(enumerated_signal_names(j)), '.log'];
        system(infer_command);
        disp(['TkT_monitoring.m: TA model inferred using TkT for signal', cell2mat(enumerated_signal_names(j))]);
        validate_command = ['java -cp tkt.jar it.unimib.disco.lta.timedKTail.ui.ValidateTraces TA.jtml invalidTraces/ > Monitor_', cell2mat(enumerated_signal_names(j)), '.log'];
        system(validate_command);
        cd (top_directory);
%         end
    end
% end

end