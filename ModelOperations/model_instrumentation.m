function [ system, model_copy_name_wo_ext, names_table, block_signal_table, LUTVar ] = model_instrumentation( model, result_path_prefix )
% Function: To create a copy of the existing model and instrument the copy
% Created:  05-03-2019
% Author:   Niveditha Manjunath

% Obtain filename and path of the model file
[ model_path, model_name_wo_ext, model_ext ] = fileparts( model );

model_path = [ model_path, '/' ];

model_copy_name_wo_ext = [ model_name_wo_ext, '_copy' ];

disp([ 'model_instrumentation.m: Copying the model ', model_name_wo_ext, '...' ]);

% Create a copy of the model for further processes in the workflow
status = model_copy( model_path, model_name_wo_ext, model_ext );

if (status == 0)
    disp([ 'model_instrumentation.m: The copy of ', model_name_wo_ext, ' was not succesful.' ]);
else 
    disp([ 'model_instrumentation.m: The copy of ', model_name_wo_ext, ' was succesful.' ]);
end

disp([ 'model_instrumentation.m: Loading the model ', model_copy_name_wo_ext, '...' ]);

system = load_system([ model_path, model_copy_name_wo_ext, model_ext ]);

disp([ 'model_instrumentation.m: Model ', model_copy_name_wo_ext, ' loaded.' ]);

% open_system([ model_path, model_copy_name_wo_ext, model_ext ]);

% Generate a custom short name of the signals and assign them as signal logging name
[ names_table, block_signal_table, LUTVar ] = model_line_rename( model_copy_name_wo_ext, system, result_path_prefix );

save_system(system);

end