function [status] = model_copy( model_path, model_name_wo_ext, model_ext )
% FUNCTION: Copy the model model_name into model_name_copy
%
% Created:  01-10-2018
% Author:   Dejan Nickovic

model_name = strcat(model_name_wo_ext, model_ext);

model_new_name = strcat(model_name_wo_ext, '_copy');
model_new_name = strcat(model_new_name, model_ext);

status = copyfile(strcat(model_path, model_name), ...
                  strcat(model_path, model_new_name));
              
end