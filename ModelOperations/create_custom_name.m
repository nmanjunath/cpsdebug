function [ name ] = create_custom_name(src_h, dst_h)
% FUNCTION: Creates a custom line name from its source and
% destination port handles
%
% Created:  03-10-2018
% Author:   Dejan Nickovic

% if the same source goes to multiple destinations
% pick the first one

if (length(dst_h) > 1)
    dst_h = dst_h(1);
end

src_block_name = get_param(src_h, 'Parent');
dst_block_name = get_param(dst_h, 'Parent');
src_port_number = num2str(get_param(src_h, 'PortNumber'));
dst_port_number = num2str(get_param(dst_h, 'PortNumber'));

name = [src_block_name, ':', src_port_number, '-->', ...
        dst_block_name, ':', dst_port_number]; 

end