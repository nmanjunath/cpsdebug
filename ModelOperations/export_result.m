function export_result( result_path, nb_model_variables, nb_test, nb_failed_test, nb_daikon_properties, nb_TkT_properties );
% FUNCTION: Export analysis parameters to a file in result path
% Created:  18-04-2020
% Author:   Niveditha Manjunath

nb_passed_test = nb_test - nb_failed_test;

write_text = {'Number of model variables: ', 'Number of tests in TestSuite: ', ...
    'Number of passed tests: ', 'Number of failed tests: ', ...
    'Number of inferred Daikon properties: ', 'Number of inferred TkT properties: '};

write_data = [nb_model_variables; nb_test; nb_passed_test; nb_failed_test; ...
    nb_daikon_properties; nb_TkT_properties];

fileID = fopen([result_path, '/SimulationParameters.txt'],'w');
for i = 1:length(write_data)
    nbytes = fprintf(fileID,'%s %d\r\n', write_text{i}, write_data(i));
end
fclose(fileID);

disp(['export_result.m: Simulation parameters are exported to the file: ', result_path, '/SimulationParameters.txt']);

end