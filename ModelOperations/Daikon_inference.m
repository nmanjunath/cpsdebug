function [ failed_test_list ] = Daikon_inference( result_path, Daikon_src, Daikon_config, daikon_source_prefix, daikon_output_invariant_prefix, combined_simout, signal_names, keep_sig, verdict )
% FUNCTION: Generate inputs for Daikon and infer invariants using Daikon
% Created:  16-01-2020
% Author:   Niveditha Manjunath

index = 1;

failed_test_list = [];

for i = 1:length(verdict)    
    if (sum(isnan(verdict{i})) == length(verdict{i}))
        test_verdict(i) = true; % If test_verdict = true at the end of the loop, the test did not violate any specifications
    else
        test_verdict(i) = false;
        failed_test_list(index) = i;
        index = index + 1;
    end
end

disp(['Daikon_inference.m: Number of failed tests: ', num2str(length(failed_test_list))]);
disp('Daikon_inference.m: Generating inputs for Daikon.');

signals_daikonWR( result_path, combined_simout, signal_names, keep_sig, failed_test_list ); % Generate files with Daikon input format

daikon_dtrace_file_list = dir([result_path, '/', daikon_source_prefix, '*.dtrace']);
daikon_decls_file_list = dir([result_path, '/', daikon_source_prefix, '*.decls']);

for j = 1:length(daikon_dtrace_file_list)
    command = ['java -cp ', Daikon_src, ' daikon.Daikon --config ', Daikon_config, ' --show_progress ', result_path, '/', daikon_dtrace_file_list(j).name, ' ', result_path, '/', daikon_decls_file_list(j).name, ' > ', result_path, '/', daikon_output_invariant_prefix, num2str(j), '.txt'];
    system(command);
end

disp('Daikon_inference.m: Properties inferred using Daikon.');

end