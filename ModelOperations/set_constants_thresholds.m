function set_constants_thresholds( file_name )
% FUNCTION: Reads constants and thresholds to be initialized from file and assigns to base workspace
% Created:  11-11-2019
% Author:   Niveditha Manjunath

assign_vars = readtable( file_name, 'ReadVariableNames', false );
for i = 1:height(assign_vars)
    evalin( 'caller', [cell2mat(assign_vars.Var1(i)), cell2mat(assign_vars.Var2(i)), cell2mat(assign_vars.Var3(i))] );
end

end