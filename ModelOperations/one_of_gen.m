function [ assertion ] = one_of_gen( one_of_invariant )
% FUNCTION: Generate 'one of' type assertions
% Created:  28-10-2018
% Author:   Niveditha Manjunath

invariant = erase(one_of_invariant,'one of');
invariant = erase(invariant,' ');
begin_index = regexp(invariant, '{');
end_index = regexp(invariant, '}');
val_list = str2num(invariant(begin_index+1: end_index-1));
sig_name = invariant(1:begin_index-1);
gen_assertion = [];
assertion = [];
for i = 1:length(val_list)
        gen_assertion = [ '( ', sig_name, ' == ', num2str(val_list(i)), ' )' ];
        if (i > 1)
            assertion = [assertion, ' or ', gen_assertion ];
        else
            assertion = gen_assertion;
        end
end


end
