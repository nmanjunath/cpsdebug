function [ ] = clear_files( path )
%FUNCTION remove temporary files from data folder
% Created:  22-09-2017
% Author:    Thomas Ferrere

disp('Clearing pre-existing result files...');

if ispc
    system('del ', path, '\*.*');
elseif isunix 
    system('rm ', path, '/*');
elseif ismac
    %% TODO: support macintosh
    disp('Platform not supported')
else
    disp('Platform not supported')
end

end

