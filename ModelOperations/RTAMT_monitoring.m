function [ robustness, verdict ] = RTAMT_monitoring( sim_time, simout, stl_spec_name, assertions, assertions_signals, assertions_signals_index, assertions_signal_type )
% FUNCTION: Monitor test outputs wrt specifications and output monitoring verdict and robustness
% Created:  16-01-2020
% Author:   Niveditha Manjunath

% Import RTAMT Python library
mod = py.importlib.import_module('rtamt');

for i = 1:length(assertions) %Number of assertions
    for k = 1:length(simout) %Number of simulated tests
        spec = mod.STLSpecification(1); % rtamt.STLSpecification(is_pure_python = true)
        spec.name = stl_spec_name(i);
        disp(['RTAMT_monitoring.m: Evaluating assertion ', cell2mat(stl_spec_name(i)), ' for Test number ', num2str(k)]);
        for j = 1:length(assertions_signals{i})
            declare_var(spec, cell2mat(assertions_signals{i}(j)), cell2mat(assertions_signal_type{i}(j)));
        end
        declare_var(spec, 'c', 'float');
        spec.spec = ['c = ', cell2mat(assertions(i))];
        %     spec.sampling_period = sim_time(2) - sim_time(1);
        parse(spec);
		pastify(spec);
        flag = 0;
        for l = 1:length(simout{k}) %Length of the signal
            my_list = py.list({py.tuple({cell2mat(assertions_signals{i}(1)), simout{k}(l,assertions_signals_index{i}(1))})});
            for m = 2:length(assertions_signals{i}) %Number of signals in the assertion
                append(my_list,py.tuple({cell2mat(assertions_signals{i}(m)), simout{k}(l,assertions_signals_index{i}(m))}));
            end
            robustness{i}(l,k) = update(spec, sim_time(l), my_list);
            if ( flag == 0 )
                if ( robustness{i}(l,k) < 0)
                    verdict{k}(i) = sim_time(l);
                    flag = 1;
                else
                    verdict{k}(i) = NaN;
                end
            end
        end
    end
end

end