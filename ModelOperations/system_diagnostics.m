function system_diagnostics( top_directory, configuration_file, result_path )
% FUNCTION: Reads constants and thresholds to be initialized from file and assigns to base workspace
% Created:  11-11-2019
% Author:   Niveditha Manjunath

%% Set inputs to system diagnostics and perform initializations

% Load file prefixes and constants
load(['Configuration/ToolInitialization.mat']); % Inputs necessary for initializing the tool (already provided)
disp('system_diagnostics.m: Tool initialization successful');

if (exist(result_path) == 0)
    mkdir(result_path);
else
    rmdir(result_path, 's');
    mkdir(result_path);
end

[model, stl_spec_file, test_suite_filename, constants_thresholds] = initialize_system_inputs( configuration_file );

%% Model instrumentation
[ system_handle, model_copy_name_wo_ext, names_table, BlockIndex, LUTVar ] = model_instrumentation( model, result_path );

%% Execute test suite and obtain simulated data
test_suite_filename_wo_ext = test_suite_filename(1:regexp(test_suite_filename,'\.')-1);
[ sim_time, combined_simout, signal_names, nb_test ] = eval(strcat(test_suite_filename_wo_ext, '( model_copy_name_wo_ext, constants_thresholds, names_table, system_handle, LUTVar );'));

%% Perform cross-correlation on simulated data to eliminate signals with high correlation
[ keep_sig ] = corrcom( combined_simout, corr_threshold );

%% Classify tests into passed or failed using RTAMT

model_spec_bool = true;
[ model_stl_spec_name, model_assertions, model_assertions_signals, model_assertions_signals_index, model_assertions_signal_type ] = RTAMT_input_gen( result_path, model_spec_bool, stl_spec_file, daikon_output_invariant_prefix, signal_names );
[ model_robustness, model_verdict ] = RTAMT_monitoring( sim_time, combined_simout, model_stl_spec_name, model_assertions, model_assertions_signals, model_assertions_signals_index, model_assertions_signal_type );
[ failed_test_list ] = Daikon_inference( result_path, Daikon_src, Daikon_config, daikon_source_prefix, daikon_output_invariant_prefix, combined_simout, signal_names, keep_sig, model_verdict );
model_spec_bool =  false;
[ daikon_stl_spec_name, daikon_assertions, daikon_assertions_signals, daikon_assertions_signals_index, daikon_assertions_signal_type ] = RTAMT_input_gen( result_path, model_spec_bool, stl_spec_file, daikon_output_invariant_prefix, signal_names );
[ failed_simout ] = extract_failed_simout(combined_simout, failed_test_list);
[ daikon_robustness, daikon_verdict ] = RTAMT_monitoring( sim_time, failed_simout, daikon_stl_spec_name, daikon_assertions, daikon_assertions_signals, daikon_assertions_signals_index, daikon_assertions_signal_type );

%% TkT Monitoring
% Generate inputs to Monitors - TkT
[ enumerated_signal_names, enumerated_signal_index ] = TkT_input_gen( signal_names, sim_time, combined_simout, failed_test_list, result_path, TkT_src );

TkT_monitoring( top_directory, result_path, combined_simout, enumerated_signal_names, enumerated_signal_index );

%% Failure Analysis
[ nb_failed_test ] = failure_analysis( result_path, names_table, nb_clusters, daikon_assertions, daikon_assertions_signals, daikon_assertions_signals_index, failed_test_list, daikon_verdict );
export_result( result_path, height(names_table), nb_test, nb_failed_test, length(daikon_assertions), length(enumerated_signal_index) );

end