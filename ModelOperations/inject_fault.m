function [ flag ] = inject_fault( model_copy_name_wo_ext, fault_list )
% FUNCTION: Inject or reset fault/s in the model by altering Step input parameters
%
% Created:  09-10-2018
% Author:   Niveditha Manjunath

%fault_list = {'RO','RI'}; % Example input for injected faults

faultBlockHandles = find_system(model_copy_name_wo_ext, 'FindAll', 'on', 'BlockType', 'Step');

for i = 1:length(fault_list)
    switch fault_list{i}
        case 'reset'
            for j = 1:length(faultBlockHandles)
                set_param(faultBlockHandles(j), 'Before', '0', 'Time', '0', 'After', '0');
            end
            disp('inject_fault: All fault parameters reset.');
        case 'LI'
            set_param(faultBlockHandles(1), 'Before', '0', 'Time', '2', 'After', '1');
            disp('inject_fault: Fault "LI" parameters set.');
        case 'LO'
            set_param(faultBlockHandles(2), 'Before', '0', 'Time', '6', 'After', '1');
            disp('inject_fault: Fault "LO" parameters set.');
        case 'RI'
            set_param(faultBlockHandles(3), 'Before', '0', 'Time', '4', 'After', '1');
            disp('inject_fault: Fault "RI" parameters set.');
        case 'RO'
            set_param(faultBlockHandles(4), 'Before', '0', 'Time', '3', 'After', '1');
            disp('inject_fault: Fault "RO" parameters set.');
        case 'H1'
            set_param(faultBlockHandles(5), 'Before', '0', 'Time', '3', 'After', '1');
            disp('inject_fault: Fault "H1" parameters set.');
        case 'H2'
            set_param(faultBlockHandles(6), 'Before', '0', 'Time', '6', 'After', '1');
            disp('inject_fault: Fault "H2" parameters set.');
        case 'H3'
            set_param(faultBlockHandles(7), 'Before', '0', 'Time', '5', 'After', '1');
            disp('inject_fault: Fault "H3" parameters set.');
        otherwise
            set_param(faultBlockHandles, 'Before', '0', 'Time', '0', 'After', '0');
            disp(['inject_fault: Input fault name', fault_list(i), ' not fould.']);
    end
end

flag = 1;

end