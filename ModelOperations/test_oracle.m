function [ test_verdict, violation_index ] = test_oracle( result_path, nb_test )
% FUNCTION: Sort tests between passed and failed
% Created:  22-08-2018
% Author:   Niveditha Manjunath

verdict_file_prefix =  [result_path, '/Monitor_'];

for i = 1:nb_test
    filename = [verdict_file_prefix, num2str(i), '.csv'];
    verdicts{i} = table2cell(readtable(filename,'Delimiter',',','ReadVariableNames',false,'HeaderLines',1));
end

test_verdict = ones(nb_test,1); %Initialize test verdict to zero

for i = 1:nb_test
    violation_index{i} = find(strcmp(verdicts{i}, 'violated'));
    if (~isempty(violation_index{i}))
        test_verdict(i,1) = 0;
    end
end

sum(test_verdict)

end