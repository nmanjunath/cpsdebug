function [ enumerated_signal_names, enumerated_signal_index ] = TkT_input_gen( signal_names, sim_time, simout, failed_test_list, result_path_prefix, TkT_src )
% FUNCTION: Transforming data to TkT input format and write to .csv files
% Created:  11-11-2019
% Author:   Niveditha Manjunath

% For analysis using TkT, we consider only enumerated signals to generate automaton

disp('TkT_input_gen.m: Generating inputs for TkT.');
% We collect the indices of enumerated signals
signal_index = 1;
for i = 1:length(signal_names)
    if( signal_names{i,3} == 1 )
        enumerated_signal_names{signal_index} = signal_names{i,1};
        enumerated_signal_index(signal_index) = i;
        signal_index = signal_index + 1;
    end
end

% Check if recorded time starts from 0, else append initial value
if (sim_time(1) == 0)
    timestamp = sim_time.*100; % Normalizing time to remove decimal (Note: improve to automatize)
else
    timestamp = (vertcat(0, sim_time).*100); % Normalizing time to remove decimal (Note: improve to automatize)
end

% Generate TkT input format traces
% for l = 1:length(simout)
%     one_simout = simout{l};
%     for i = 1:length(one_simout)
for i = 1:length(simout)
    for j = 1:length(enumerated_signal_index)
        disp(['TkT_input_gen.m: Processing result of test ', num2str(i), ' for signal ', cell2mat(enumerated_signal_names(j))]);
        %             if ( keep_sig(enumerated_signal_index(j)) == 1 ) % Use only signals that are not highly correlated
        signal_information = [];
        write_signal_information = [];
        enumerated_signals{i}(:,j) = simout{i}(:,enumerated_signal_index(j)); % Store the enumerated signals from each test simulation (simout{i})
        signal_begin_value = enumerated_signals{i}(1,j);
        signal_change_array = vertcat(0,diff(enumerated_signals{i}(:,j)));
        signal_change_index = find(signal_change_array);
        signal_change_begin_index = vertcat(1, [signal_change_index]);
        signal_change_end_index = vertcat([signal_change_index]-1, length(timestamp));
        signal_index = sort(vertcat(signal_change_begin_index,signal_change_end_index));
        for k = 1:length(signal_index)
            if (mod(k,2) == 1) % If k is an odd number, signal value begins at the index
                signal_value(k) = int32(enumerated_signals{i}(signal_index(k),j));
                signal_timestamp(k) = timestamp(signal_index(k));
                signal_information{k,1} = ['bla;', cell2mat(enumerated_signal_names(j)), '_' num2str(signal_value(k)), '.0;B;', num2str(signal_timestamp(k)), ';a'];
            else if (mod(k,2) == 0) % If k is an odd number, signal value ends at the index
                    signal_value(k) = int32(enumerated_signals{i}(signal_index(k),j));
                    signal_timestamp(k) = timestamp(signal_index(k));
                    signal_information{k,1} = ['bla;', cell2mat(enumerated_signal_names(j)), '_' num2str(signal_value(k)), '.0;E;', num2str(signal_timestamp(k)), ';a'];
                end
            end
        end
        write_signal_information = cell2table(['START'; signal_information; 'STOP']);
        % If the test number does not exist in the FailedTestList, this
        % implies that the test had passed and must be written as a
        % valid trace, else it is written as an invalid trace
        if isempty(find(failed_test_list == i))
            output_path = [result_path_prefix, '/TkT/', cell2mat(enumerated_signal_names(j)), '/validTraces'];
        else
            output_path = [result_path_prefix, '/TkT/', cell2mat(enumerated_signal_names(j)), '/invalidTraces'];
        end
        % Copying TkT jar file into folder %%Note: Avoid when possible
        if (exist(output_path) == 0)
            mkdir(output_path);
            copyfile (TkT_src, [result_path_prefix, '/TkT/', cell2mat(enumerated_signal_names(j))]);
        end
        writetable(write_signal_information, [output_path, '/Test', num2str(i), '_', cell2mat(enumerated_signal_names(j)), '.csv'], 'WriteVariableNames', 0);
        %             end
    end
end
% end

end