function trace_diagnostic( result_path, test_numbers, AMT_src, stl_file, alias_file, input_file_prefix, output_file_prefix )
% FUNCTION: Calls AMT to perform trace diagnostics
% Created:  31-08-2017
% Author:   Thomas Ferrere

input_path_prefix = [result_path, '\', input_file_prefix];
output_path_prefix = [result_path, '\', output_file_prefix];

for i=1:length(test_numbers)
    disp(['Trace diagnostics for test number ', num2str(test_numbers(i))]);
    input_file = [input_path_prefix, num2str(test_numbers(i)), '.csv'];
%     amt_file = [input_path_prefix, num2str(test_numbers(i)), '_AMT.csv'];
%     concatenate_files([result_path, '\', header_file], input_file, amt_file);
    output_file = [output_path_prefix, num2str(test_numbers(i)), '.csv'];
    command = ['java -jar ', AMT_src, ' -x ', stl_file, ' -a ', alias_file, ' -s ', input_file, ' -d epoch -r ', output_file];
    system(command);
    
end
end