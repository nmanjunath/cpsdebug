% Plotting transition signal from all passing test (all simulations)
figure(1);
time_var = [0:0.04:30]';
for i = 1:length(simout)
    if ~((i == 59)|(i == 60)|(i == 88))
        plotSignal_transition = simout{i}(:,10);
        plotSignal_gear = simout{i}(:,11);
        subplot(2,1,1)
        plot(time_var, plotSignal_transition);
        hold on;
        subplot(2,1,2)
        plot(time_var, plotSignal_gear);
        hold on;
    end
end
xlabel('Time (s)');
ylabel('Transition Number');
set(gcf,'Color','w');
% legend('');
hold off;

for i = 1:length(simout)    
    if(find(simout{i}(:,10) == 4))
        i
    end
end