function [ stlfilename ] = AMT_stl( result_path, header_var, stl_header_var, invariant_filename_prefix, stl_file_output )
% FUNCTION: Generate .stl input file for AMT using invariants learnt from Daikon
% Created:  28-10-2018
% Author:   Niveditha Manjunath

% AMT requires several input files to perform monitoring and diagnostics
% 1. Test input as .csv file
% 2. formula.alias
% 3. formula.stl


% Generate content of formula.stl file as a text file
% and rename as formula.stl
% Variable declaration and assertion definition are generated separately
% and appended together to create formula.stl file


for i = 1:length(header_var)
    stldef_var{i,1} = [stl_header_var{1,i}, header_var{1,i}, ';'];
end


inv_files = dir(fullfile([pwd, '\', result_path], [invariant_filename_prefix,'*.txt']));

index = 1;
stl = [];

if (~isempty(inv_files))
    [ assertions ] = assertion_gen( inv_files );  

    % Note that invariant of type 'one of' must be manually handled before
    % using it for monitoring
    % Also, since AMT requires '0x' to be present before all 'bus64' type
    % values, this is to be entered manually as well
    
    for i = 1:length(assertions)
        stl{index,1} = ['assertion    mySafetyProperty', num2str(i), ':'];
        index = index + 1;
        stl{index,1} = ['    ', assertions{i}];
        index = index + 1;
    end
end

% While generating formula.stl for factory spec, if no invariant file input
% exists, the function then generates only the variable declaration and
% generates the file

if (isempty(stl))
    stlfile_var = stldef_var;
    disp('No invariants were printed to formula.stl');
else
    stlfile_var = vertcat(stldef_var, stl);
end

stlfile = table(stlfile_var);
writetable(stlfile, 'stlfile.txt', 'WriteVariableNames', false);
stlfilename = [result_path, '\', stl_file_output];
movefile('stlfile.txt', stlfilename);

end