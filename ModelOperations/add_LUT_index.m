function[ simout ] = add_LUT_index( simout, signal_names, names_table, LUTVar )
% FUNCTION: Generate and add LUT index of EngineTorque block as a signal
% Created:  31-01-2019
% Author:   Niveditha Manjunath
% Note:     Variable emap from Model Workspace is not currently in use in
%           this function

LUT_index = 1; % Initialize index to keep track of number of generated LUT signals
% Generating LUT index signals

% Generating LUT index signals for EngineTorque

m = 10; %No. of rows in the LUT
n = 11; %No. of columns in the LUT

[nb_samples, nb_signals] = size(simout{1});
names_table_height = height(names_table);
LUT_index_signal = [];

for i = 1:length(simout)
    for j = 1:nb_samples
        %Index = x+n*y; where x is the column index and y is the row index
        x = (find(nevec <= simout{i}(j,3),1,'last'))-1; % {'s6' - {'autotrans_mod04_copy/Engine/Integrator:1-->autotrans_mod04_copy/Engine/EngineTorque:2'}}
        y = (find(thvec <= simout{i}(j,5),1,'last'))-1; % {'s7' - {'autotrans_mod04_copy/Engine/Throttle:1-->autotrans_mod04_copy/Engine/EngineTorque:1'}}
        LUT_index_signal(j) = x+(n*y);
    end
    simout{i}(:,nb_signals+1) = LUT_index_signal;
end

% Append Index signals to signal_names variable
% Add EngineTorque LUT index as signal
signal_names{nb_signals+1,1} = ['s', num2str(names_table_height+LUT_index)];
signal_names{nb_signals+1,2} = 1;
signal_names{nb_signals+1,3} = 1;
LUT_index = LUT_index + 1;

% Generating LUT index signals for ThresholdCalculation - Up

m = 6; %No. of rows in the LUT
n = 4; %No. of columns in the LUT

[nb_samples, nb_signals] = size(simout{1});
names_table_height = height(names_table);
LUT_index_signal = [];
gear_index = [1 2 3 4];

for i = 1:length(simout)
    for j = 1:nb_samples
        %Index = x+n*y; where x is the column index and y is the row index
        x = (find(gear_index <= simout{i}(j,11),1,'last'))-1; % {'s45' - {'autotrans_mod04_copy/ShiftLogic:1-->autotrans_mod04_copy/gear:1'}}
        if ( isempty(x) ) %% Note: Verify if this is correct
            x = 0;
        end
        y = (find(upth <= simout{i}(j,8),1,'last'))-1; % {'s37' - {'autotrans_mod04_copy/In1:1-->autotrans_mod04_copy/Engine:2'}}
        if ( isempty(y) ) %% Note: Verify if this is correct
            y = 0;
        end
        LUT_index_signal(j) = x+(n*y);
    end
    simout{i}(:,nb_signals+1) = LUT_index_signal;
end

% Append Index signals to signal_names variable
% Add EngineTorque LUT index as signal
signal_names{nb_signals+1,1} = ['s', num2str(names_table_height+LUT_index)];
signal_names{nb_signals+1,2} = 2;
signal_names{nb_signals+1,3} = 1;
LUT_index = LUT_index + 1;

% Generating LUT index signals for ThresholdCalculation - Down

m = 6; %No. of rows in the LUT
n = 4; %No. of columns in the LUT

[nb_samples, nb_signals] = size(simout{1});
names_table_height = height(names_table);
LUT_index_signal = [];
gear_index = [1 2 3 4];

for i = 1:length(simout)
    for j = 1:nb_samples
        %Index = x+n*y; where x is the column index and y is the row index
        x = (find(gear_index <= simout{i}(j,11),1,'last'))-1; % {'s45' - {'autotrans_mod04_copy/ShiftLogic:1-->autotrans_mod04_copy/gear:1'}}
        if ( isempty(x) ) %% Note: Verify if this is correct
            x = 0;
        end
        y = (find(downth <= simout{i}(j,8),1,'last'))-1; % {'s37' - {'autotrans_mod04_copy/In1:1-->autotrans_mod04_copy/Engine:2'}}
        if ( isempty(y) ) %% Note: Verify if this is correct
            y = 0;
        end
        LUT_index_signal(j) = x+(n*y);
    end
    simout{i}(:,nb_signals+1) = LUT_index_signal;
end

% Append Index signals to signal_names variable
% Add EngineTorque LUT index as signal
signal_names{nb_signals+1,1} = ['s', num2str(names_table_height+LUT_index)];
signal_names{nb_signals+1,2} = 2;
signal_names{nb_signals+1,3} = 1;
LUT_index = LUT_index + 1;



end