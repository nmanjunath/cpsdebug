function [ ] = concatenate_files ( in1 , in2 , out0 )
%FUNCTION concatenates two files using system command
% Created:	21-09-2017
% Author:	Thomas Ferrere

if ispc
    system(['type ',in1,' ',in2,' >> ',out0]);
elseif isunix 
    system(['touch ',out0]);
    system(['cat ',in1,' ',in2,' >> ',out0]);
elseif ismac
    system(['touch ',out0]);
    system(['cat ',in1,' ',in2,' >> ',out0]);
else
    disp('Platform not supported');
end