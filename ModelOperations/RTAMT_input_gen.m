function [ stl_spec_name, assertions, assertions_signals, assertions_signals_index, assertions_signal_type ] = RTAMT_input_gen( result_path, model_spec_bool, stl_spec_file, invariant_filename_prefix, signal_names )
% FUNCTION: Generate inputs for rtamt, the invariants may either have been provided in a text file as model spec or inferred using Daikon
% Created:  10-01-2020
% Author:   Niveditha Manjunath

disp('RTAMT_input_gen.m: Generating inputs for RTAMT...');

% model_spec_bool = true, the invariants being processed currently are user provided
% model_spec_bool = false, the invariants being processed currently are learned using Daikon

if (model_spec_bool == false)
    inv_files = dir(fullfile([pwd, '/', result_path, '/', invariant_filename_prefix,'*.txt']));
    if (~isempty(inv_files))
        [ assertions ] = assertion_gen( inv_files );
    end
else
    file_data = fileread( stl_spec_file );
    index = 1;
    inv_begin_index = [];
    inv_end_index = [];
    if (~isempty(file_data))
        inv_index = regexp(file_data,[char(13)]);
        inv_begin_index = [1, inv_index+2];
        inv_end_index = [inv_index-1, length(file_data)];
        for j = 1:length(inv_begin_index)
            assertions{index,1} = file_data(inv_begin_index(j):inv_end_index(j));
            index = index + 1;
        end
    end
end

stl_spec_name = [];
for i = 1:length(assertions)
    stl_spec_name{i,1} = ['Monitor_Safety_Property', num2str(i)];
end

[ assertions_signals, assertions_signals_index, assertions_signal_type ] = assertion_signal_list( assertions, signal_names );



end