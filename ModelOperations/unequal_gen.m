function [ assertion ] = unequal_gen( unequal_invariant )
% FUNCTION: Generate '!=' (inequality) type assertions
% Created:  28-10-2018
% Author:   Niveditha Manjunath

assertion = strrep(unequal_invariant, '!', '!=');

end