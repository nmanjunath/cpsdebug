function [ nb_failed_test ] = failure_analysis( result_path, names_table, nb_clusters, assertions, assertions_signals, assertions_signals_index, failed_test_list, daikon_verdict )
% FUNCTION: Combining diagnostics reports from RTAMT and TkT
% Created:  21-01-2020
% Author:   Niveditha Manjunath

%% Processing Daikon verdict to extract violations

disp('failure_analysis.m: Analyzing failures from Daikon analysis...');

for i = 1:length(failed_test_list)
    failed_test_accumulator_Daikon{i} = ['Test', num2str(failed_test_list(i))];
end

% Obtain a list of violated signals and violation time per test
for i = 1:length(failed_test_list)
    spec_index = 1;
    for j = 1:length(daikon_verdict{i})
        if (~isnan(daikon_verdict{(i)}(j)))
            violated_spec_index{i}(spec_index) = j;
            violated_spec_time{i}(spec_index) = daikon_verdict{(i)}(j);
            spec_index = spec_index + 1;
        end
    end
end

% Obtain a list of violated signals and violation time per test - Daikon
for i = 1:length(failed_test_list)
    signal_index = 1;
    failed_sig_per_test_Daikon = [];
    failed_sig_timestamp_Daikon = [];
    for j = 1:length(violated_spec_index{i})
        failed_sig_per_spec_Daikon = assertions_signals{violated_spec_index{i}(j)};
        for k = 1:length(failed_sig_per_spec_Daikon)
            failed_sig_per_test_Daikon{signal_index,1} = failed_sig_per_spec_Daikon{k}; % Capture failed signal name
            failed_sig_per_test_Daikon{signal_index,2} = violated_spec_time{i}(j); % Capture time of spec violation
            failed_sig_timestamp_Daikon{signal_index} = violated_spec_time{i}(j); % Capture time of spec violation
            signal_index = signal_index + 1;
        end
    end
    [ind_m, ind_n] = size(failed_sig_per_test_Daikon);
    if (ind_m > 0)
        [ ~, idx ] = unique( strcat( failed_sig_per_test_Daikon(:,1), failed_sig_per_test_Daikon(:,2)), 'stable');
        ind_mat = 1;
        for m = 1:length(failed_sig_per_test_Daikon(idx,:))
%             if ~(strcmp(failed_sig_per_test_Daikon(idx(m),1),failed_sig_per_test_Daikon(idx(m+1),1)))
                failed_signals_per_test_Daikon{i}(ind_mat) = failed_sig_per_test_Daikon(idx(m),1);
                failed_signals_timestamp_Daikon{i}(ind_mat) = cell2mat(failed_sig_per_test_Daikon(idx(m),2));
                ind_mat = ind_mat + 1;
%             end
        end
    end
end

%% Processing TkT report files to extract violations

disp('failure_analysis.m: Analyzing failures from TkT analysis...');

TkT_report_files = dir(fullfile([pwd, '/', result_path,'/TkT/*/TkT.validationResults.csv']));
file_names = natsortfiles({TkT_report_files.name})';
failed_test_accumulator_TkT = [];
acc_index = 1;
for i = 1:length(file_names)
    file_path = TkT_report_files(i).folder;
    file_data{i} = readtable([file_path, '/', file_names{i}],'Delimiter',',');
    test_trace_names{i} = file_data{i}.TracePath;
    for j = 1:length(test_trace_names{i})
        test_name_begin_index{i,j} = regexp(test_trace_names{i}(j),'[\\/]');
        test_name_end_index{i,j} = regexp(test_trace_names{i}(j),['_']);
        test_name_temp = cell2mat(test_trace_names{i}(j));
        failed_test_names{i,j} = test_name_temp(cell2mat(test_name_begin_index{i,j})+1:cell2mat(test_name_end_index{i,j})-1);
        if (~ismember(failed_test_names{i,j}, failed_test_accumulator_TkT))
            failed_test_accumulator_TkT{acc_index} = failed_test_names{i,j};% Accumulate a union of failed tests for all signals
            acc_index = acc_index + 1;
        end
        signal_name_begin_index{i,j} = regexp(test_trace_names{i}(j),['_']);
        signal_name_end_index{i,j} = regexp(test_trace_names{i}(j),['.csv']);
        failed_signal_names{i,1} = test_name_temp(cell2mat(signal_name_begin_index{i,j})+1:cell2mat(signal_name_end_index{i,j})-1); % record signal name just once per TkT signal folder
    end
    violation_timestamp{i} = (file_data{i}.EventTimestamp)/100;
end

failed_test_accumulator_TkT = unique(failed_test_accumulator_TkT);
for i = 1:length(violation_timestamp)
    for j = 1:length(violation_timestamp{i})
        signal_violation_timestamp{i,j} = violation_timestamp{i}(j);
    end
end

% Obtain a list of violated signals and violation time per test - TkT
for i = 1:length(failed_test_accumulator_TkT)
    test_index = 1;
    failed_sig_per_test_TkT = [];
    failed_sig_timestamp_TkT = [];
    test_signal_index = strcmp(failed_test_accumulator_TkT{i}, failed_test_names);
    for j = 1:length(failed_signal_names)
        if (~isempty(failed_signal_names{j}))
            if (sum(test_signal_index(j,:)) == 1)
                failed_sig_per_test_TkT{test_index} = failed_signal_names{j};
                failed_sig_timestamp_TkT{test_index} = cell2mat(signal_violation_timestamp(j,find(strcmp(failed_test_accumulator_TkT{i}, failed_test_names(j,:)))));
                test_index = test_index + 1;
            end
        end
    end
    failed_signals_per_test_TkT{i} = failed_sig_per_test_TkT;
    failed_signals_timestamp_TkT{i} = failed_sig_timestamp_TkT;
end

%% Combine failure analysis from both Daikon and TkT

disp('failure_analysis.m: Combining failure analysis from Daikon and TkT...');

nb_Daikon_failed_tests = length(failed_test_accumulator_Daikon);
nb_TkT_failed_tests = length(failed_test_accumulator_TkT);
failed_test_accumulator = unique([failed_test_accumulator_Daikon, failed_test_accumulator_TkT],'stable');

test_match_index_Daikon{length(failed_test_accumulator)} = [];

for i = 1:length(failed_test_accumulator_Daikon)
    test_match_index_Daikon{i} = find(strcmp(failed_test_accumulator_Daikon{i},failed_test_accumulator));
end

test_index_Daikon = cell2mat(test_match_index_Daikon);

test_match_index_TkT{length(failed_test_accumulator)} = [];

for i = 1:length(failed_test_accumulator_TkT)
    test_match_index_TkT{i} = find(strcmp(failed_test_accumulator_TkT{i},failed_test_accumulator));
end

test_index_TkT = cell2mat(test_match_index_TkT);

failed_signals_per_test{length(failed_test_accumulator)} = [];
failed_signals_timestamp{length(failed_test_accumulator)} = [];

for i = 1:length(failed_test_accumulator)
    failed_signals_timestamp_temp = [];
    if (~isempty(find(test_index_Daikon == i)))
        failed_signals_per_test{i} = [failed_signals_per_test{i}, failed_signals_per_test_Daikon{find(test_index_Daikon == i)}];
        failed_signals_timestamp_temp = horzcat( failed_signals_timestamp_temp, failed_signals_timestamp_Daikon{find(test_index_Daikon == i)} );
    end
    if (~isempty(find(test_index_TkT == i)))
        failed_signals_per_test{i} = [failed_signals_per_test{i}, failed_signals_per_test_TkT{find(test_index_TkT == i)}];
        failed_signals_timestamp_temp = horzcat( failed_signals_timestamp_temp, cell2mat(failed_signals_timestamp_TkT{find(test_index_TkT == i)}) );
    end
    failed_signals_timestamp{i} = failed_signals_timestamp_temp;
end

remove_rows_from_table = [];

for i = 1:length(failed_test_accumulator)
    failure_analysis_table_SignalIndex = failed_signals_per_test{i}';
    failure_analysis_table_ViolationTime = failed_signals_timestamp{i}';
    failure_analysis_table_SignalName = [];
    failure_analysis_table_BlockName = [];
    for j = 1:length(failed_signals_per_test{i})
        failure_analysis_table_SignalName{j,1} = cell2mat(names_table.FullName(find(strcmp(cell2mat(failed_signals_per_test{i}(j)),names_table.ShortName))));
        failure_analysis_table_BlockName{j,1} = cell2mat(names_table.BlockName(find(strcmp(cell2mat(failed_signals_per_test{i}(j)),names_table.ShortName))));
    end
    failure_analysis_table_temp = sortrows(table(failure_analysis_table_SignalIndex, failure_analysis_table_SignalName, failure_analysis_table_BlockName, failure_analysis_table_ViolationTime));
    [unique_test_names{i}, test_index_in_actual{i}, test_index_in_unique{i}] = unique(failure_analysis_table_temp.failure_analysis_table_SignalIndex);
    remove_ind = 1;
    remove_rows_from_table = [];
    for j = 1:length(failure_analysis_table_temp.failure_analysis_table_SignalIndex)
        if (isempty(find(test_index_in_actual{i} == j)))
            remove_rows_from_table(remove_ind) = j;
            remove_ind = remove_ind + 1;
        end
    end
    if (exist('remove_rows_from_table','var'))
        failure_analysis_table_temp(remove_rows_from_table,:) = [];
    end
    failure_analysis_table{i} = failure_analysis_table_temp;
    if (exist([result_path, '/FailureAnalysis/']) == 0)
        mkdir([result_path, '/FailureAnalysis/']);
    end
    writetable(failure_analysis_table{i}, [result_path, '/FailureAnalysis/', failed_test_accumulator{i}, '.csv']);
end

% Compute total number of failed tests
nb_failed_test = length(failed_test_accumulator);

disp(['failure_analysis.m: Failure analysis result can be found in the folder: ', result_path, '/FailureAnalysis']);

end