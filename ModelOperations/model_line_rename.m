function [ names_table, BlockIndex, LUTVar ] = model_line_rename( model_name, system, result_path )
% FUNCTION: Renames all the lines in the model
% Created:  01-10-2018
% Author:   Dejan Nickovic

line_handles = find_system(system, 'FindAll', 'on', 'Type', 'Line');
block_handles = find_system(system, 'FindAll', 'on', 'Type', 'Block');

% Assigning a unique BlockIndex to each block in the model
for i = 1:length(block_handles)
    set_param(block_handles(i),'UserData',i);
    block_UserData{i,1} = get_param(block_handles(i),'UserData');
end

% We distinguish between different signals:
% 1) Single signal
% 2) Array of signals
% 3) Virtual bus
% 4) Non-Virtual bus (PositionBus)
% This list may not be exhaustive
% The signal type can be infered from its source port

% Single signal:
% CompiledPortDataType: 'double', 'some_custom_type', etc.
% CompiledPortWidth: 1
% CompiledPortDimensions: [1 1]
% CompiledBusType: 'NOT_BUS'

% Mux (array) signal:
% CompiledPortDataType: 'double', 'some_custom_type', etc.
% CompiledPortWidth: n
% CompiledPortDimensions: [1 n]
% CompiledBusType: 'NOT_BUS'

% Virtual bus:
% CompiledPortDataType: 'double', 'some_custom_type', etc.
% CompiledPortWidth: n
% CompiledPortDimensions: [-2 ......]
% CompiledBusType: 'VIRTUAL_BUS'

% Non Virtual bus:
% CompiledPortDataType: 'PositionBus'
% CompiledPortWidth: 1
% CompiledPortDimensions: [1 1]
% CompiledBusType: 'NON_VIRTUAL_BUS'

% In order to read information about signal ports, we need to compile the
% model
eval(strcat(model_name, "([], [], [], 'compile');"));
line_log = [];
source_port_data_type = [];
source_port_data_type_val = [];
for(i=1:length(line_handles))
    line_handle = line_handles(i);
    
    source_port_handle = get_param(line_handle, 'SrcPortHandle');
    source_port_width = get_param(source_port_handle, 'CompiledPortWidth');
    source_port_dim = get_param(source_port_handle, 'CompiledPortDimensions');
    source_port_bus_type = get_param(source_port_handle, 'CompiledBusType');
    
    % We want to log only signals having width 1, port dimensions [1 1]
    % and bus type 'NOT_BUS'
    flag = 0;
    if(source_port_width == 1 && source_port_dim(1) == 1 && ...
            source_port_dim(2) == 1 && strcmp(source_port_bus_type, 'NOT_BUS') == 1)
        flag = 1;
    end
    
    source_port_data_type_val = get_param(source_port_handle, 'CompiledPortDataType');
        
    % We do not want to log signals that are of type fcn_call (function
    % calls)
    if(strcmp(source_port_data_type_val, 'fcn_call'))
        flag = 0;
    end
    source_port_data_type{i,1} = source_port_data_type_val;
    
    line_log = [line_log flag];
%     source_port_data_type{i,1} = get_param(source_port_handle, 'CompiledPortDataType');
end
eval(strcat(model_name, "([], [], [], 'term');"));

disp([ 'model_line_rename.m: Instrumenting model ', model_name, '...' ]);

j = 1;
ShortName = [];
FullName = [];
BlockIndex = [];
BlockName = [];
k = 1; % index variable for Hash map
blockMap = containers.Map;
n = 1; % index variable for block_signal_array

for(i=1:length(line_handles))
    line_handle = line_handles(i);
    flag = line_log(i);
    
    source_port_handle = get_param(line_handle, 'SrcPortHandle');
    dst_port_handle = get_param(line_handle, 'DstPortHandle');
    source_block_handle = get_param(line_handle, 'SrcBlockHandle');
    dst_block_handle = get_param(line_handle, 'DstBlockHandle');
    if (length(dst_block_handle) > 1) % Destination blocks can be more than 1
        dst_block_handle = dst_block_handle(1);
    end
    
    % We log only the signals with true flag
    if (flag == 1)
        set_param(source_port_handle, 'DataLogging', 'on');
        set_param(source_port_handle, 'DataLoggingNameMode', 'Custom');
        
        % short name - for example s53
        % full name - 'inport_name-->outport_name'
        full_name = create_custom_name(source_port_handle, dst_port_handle);
        short_name = strcat('s',num2str(j));
        block_name = get_param(line_handle,'Parent');
        signal_type = source_port_data_type{i,1};
        block_handle = getSimulinkBlockHandle(get_param(line_handle,'Parent'));
        if(isKey(blockMap, block_name) == 1)
            block_index = values(blockMap, { block_name });
        else
            if (block_handle == -1) % Block handle does not exist for top level of the model
                block_index = 0; % Hence we assign the block index as 0
            else
                block_index = get_param(block_handle,'UserData');
            end
            blockMap(block_name) = block_index;
            k = k + 1;
        end
        %         if(isKey(blockMap, block_name) == 1)
        %             block_index = values(blockMap, { block_name });
        %         else
        %             block_index = k;
        %             blockMap(block_name) = block_index;
        %             k = k + 1;
        %         end
        
        
        ShortName{j,1} = short_name;
        FullName{j,1} = regexprep(full_name, char(10),'');
        if iscell(block_index)
            BlockIndex{j,1} = cell2mat(block_index);
        else
            BlockIndex{j,1} = block_index;
        end
        BlockName{j,1} = regexprep(block_name, char(10),'');
        m = enumeration(signal_type);
        if (~isempty(m))
            SignalType{j,1} = 1;
        else
            switch signal_type
                case ('double')
                    SignalType{j,1} = 0;
                case ('boolean')
                    SignalType{j,1} = 2;
                case ('int')
                    SignalType{j,1} = 3;
                otherwise
                    SignalType{j,1} = 4;
            end
        end
        
        % we log the signals with the short name
        % but keep the full name in a table for traceability
        set_param(source_port_handle, 'DataLoggingName', short_name);
        
        % Create an array of signals with its Source/Desitination and Parent block
        % Format: Parent Block, Block, Signal Name, 'in'/'out'
        block_signal_array{n,1} = block_index;
        block_signal_array{n,2} = get_param(source_block_handle,'UserData');
        block_signal_array{n,3} = short_name;
        block_signal_array{n,4} = 'out';
        n = n + 1;
        
        block_signal_array{n,1} = block_index;
        block_signal_array{n,2} = get_param(dst_block_handle,'UserData');
        block_signal_array{n,3} = short_name;
        block_signal_array{n,4} = 'in';
        n = n + 1;
        
%         % Saving (line_handle,ShortName) pairs for LUT signal
%         LineHandle_for_LUT{j} = line_handle;
%         ShortName_for_LUT{j} = short_name;
        
        j = j + 1;
    end
end


% Extract variable names of LUT blocks in the model
% These extracted variables will then be used create LUT index variables
all_blocks = find_system(system, 'FindAll', 'on');
index = 1;
signal_number = length(ShortName);
RowIndexVar{index,1} = [];
ColumnIndexVar{index,1} = [];
TableIndexVar{index,1} = [];
InSignalNameVar{index,1} = [];
LUTNameVar{index,1} = [];
ParentBlockVar{index,1} = [];
ShortNameVar{index,1} = [];
FullNameVar{index,1} = [];
BlockIndexVar{index,1} = [];
SignalTypeVar{index,1} = [];
BlockNameVar{index,1} = [];

for i = 1:length(all_blocks)
    if (isfield(get_param(all_blocks(i),'ObjectParameters'),'BlockType'))
        if strcmp(get_param(all_blocks(i),'BlockType'), 'Lookup2D')
            RowIndexVar{index,1} = get_param(all_blocks(i),'RowIndex');
            ColumnIndexVar{index,1} = get_param(all_blocks(i),'ColumnIndex');
            TableIndexVar{index,1} = get_param(all_blocks(i),'Table');
            InSignalNameVar_LineHandles = get_param(all_blocks(i), 'LineHandles');
            for j = 1:length(InSignalNameVar_LineHandles.Inport)
                InSignalNameVar_InportNameHandle = InSignalNameVar_LineHandles.Inport(j);
                InSignalNameVar{index,j} = get(InSignalNameVar_InportNameHandle,'UserSpecifiedLogName');
            end
            LUTNameVar{index,1} = get_param(all_blocks(i),'Name');
            ParentBlockVar{index,1} = get_param(all_blocks(i),'Parent');
            ShortNameVar{index,1} = ['s', num2str(signal_number + index)];
            FullNameVar{index,1} = regexprep([ParentBlockVar{index,1}, '/', LUTNameVar{index,1}, '_LUTindex'],char(10),'');
            BlockIndexVar{index,1} = 0;
            SignalTypeVar{index,1} = 1;
            BlockNameVar{index,1} = regexprep([ParentBlockVar{index,1}, '/', LUTNameVar{index,1}],char(10),'');
            index = index + 1;
        end
    end
end

LUTVar = table(LUTNameVar, ParentBlockVar, RowIndexVar, ColumnIndexVar, TableIndexVar, InSignalNameVar);

save_system(system,[],'OverwriteIfChangedOnDisk',true);

% Combine extracted logged signals with LUT index signals
ShortName = vertcat( ShortName, ShortNameVar );
FullName = vertcat( FullName, FullNameVar );
BlockIndex = vertcat( BlockIndex, BlockIndexVar );
SignalType = vertcat( SignalType, SignalTypeVar );
BlockName = vertcat( BlockName, BlockNameVar );


% Save the table that relates short to full names
names_table = table(ShortName, FullName, BlockIndex, SignalType, BlockName);
block_signal_table = cell2table(block_signal_array);

% Create a table with all Block numbers and their respective Input and Output signals
writetable(names_table, [result_path, '/names_table.csv']);
writetable(block_signal_table, [result_path, '/signal_block_table.csv']);
save_system(system);

disp([ 'model_line_rename.m: Model ', model_name, ' instrumented.' ]);

end