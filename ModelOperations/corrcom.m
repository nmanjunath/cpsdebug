function [ keep_sig ] = corrcom( simout, corr_threshold )
% FUNCTION: Compute correlation and further
% Created:  27-07-2018
% Author:   Niveditha Manjunath

disp('corrcom.m: Computing cross correlation matrices');

% Get the number of simulations
nb_sim = length(simout);

% We assume that each simulation has the same number of signals and samples
% We now get the number of signals
[nb_samples nb_signals] = size(simout{1});

% Matrix of nb_signals x nb_signals for the cross-correlation analysis, initalized to 0


% We compute the correlation coefficients
for i = 1:nb_sim
    variance{i} = var(simout{i});
    covariance{i} = cov(simout{i});
    dispersion{i} = diag(covariance{i});
    std_dev{i} = sqrt(dispersion{i});
    corr_coeff = corrcoef(simout{i});
    corr_coeffs{i} = corr_coeff;
end

% We now have nb_sims correlation coefficient matrices, 
% each of size nb_signals x nb_signals
% We now create a single nb_signals x nb_signals matrix
% where each (i,j) cell is the minimum (worst case) 
% of the individual simulation (i,j) cells
min_corr_matrix = ones(nb_signals,nb_signals);
for i = 1:nb_sim
    min_corr_matrix = min(min_corr_matrix, abs(corr_coeffs{i}), 'includenan');
end

% Now we have our correlation coefficient matrix summary
% We remove all signals sj that have correlation to some si
% in [0.99,1]
keep_sig = ones(1,nb_signals);
for i = 2:nb_signals
    for j = 1:i-1
        if (min_corr_matrix(i,j) >= corr_threshold && min_corr_matrix(i,j) <= 1)
            keep_sig(i) = 0;     
        end
    end
end

% keep_sig(10) = 1; %s32 (transitionlog)
% keep_sig(11) = 1; %s45 (gear)
% keep_sig(33) = 1; %s40
% keep_sig(32) = 1;
% keep_sig(34) = 1;
% keep_sig(35) = 1; % LUT s53
% keep_sig(36) = 1; % LUT s54
% keep_sig(37) = 1; % LUT s55

end