function [  header_var, stl_header_var, header_wr ] = AMT_header( result_path, signal_names, keep_sig, header_file_name )
% FUNCTION: Generate header for input AMT .csv files
% Created:  28-10-2018
% Author:   Niveditha Manjunath

% AMT requires several input files to perform monitoring and diagnostics
% 1. Test input as .csv file
% 2. formula.alias
% 3. formula.stl

% Our process flow generates .csv files with signals (data values) alone,
% and as AMT also requires a header of format below in the input file,
% % time, si, ...., sn
% % double, double, ..., double
% we generate the necessary header here

index = 1;
for i = 1:length(signal_names)
    if (keep_sig(i) == 1)
        header_var{1,index} = signal_names{i,1};
        if ( signal_names{i,3} == 0 )
            header_var{2,index} = 'double';
            stl_header_var{1,index} = 'real ';
        else if ( signal_names{i,3} == 1 || signal_names{i,3} == 2 || signal_names{i,3} == 3 )
                header_var{2,index} = 'bus64';
                stl_header_var{1,index} = 'bus 64 ';
            else
                header_var{2,index} = 'double';
                stl_header_var{1,index} = 'real ';
            end
        end
        index = index + 1;
    end
end
header_col1{1,1} = 'time';
header_col1{2,1} = 'double';
header_wr = horzcat(header_col1, header_var);
header = table(header_wr);
writetable(header, [result_path, '\', header_file_name], 'Delimiter', ',', 'WriteVariableNames', false);


end