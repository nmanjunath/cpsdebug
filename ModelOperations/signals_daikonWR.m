function signals_daikonWR( result_path, simout, signal_names, keep_sig, failed_tests )
% FUNCTION: Write signals in Daikon input format
% Created:  18-10-2018
% Author:   Niveditha Manjunath
% Note:     Add a post-processing step to delete generated empty files

% Get the number of signals and the number of samples per simulation
[ nb_samples, nb_signals ] = size(simout{1});

% Fetch the number of groups from the names table
% which already has a column with group numbers
group_nb = unique([signal_names{:,2}]);


% For each unique group number, associate signals that belong to that group
for i = 1:length(group_nb) % Group number
    % We need to generate Daikon inputs
    % 1) The .decls class file
    % 2) The .dtrace trace file
    fileID_class = fopen([pwd, '/', result_path, '/tracefile', num2str(group_nb(i)), '.decls'], 'w');
    fileID_trace = fopen([pwd, '/', result_path, '/tracefile', num2str(group_nb(i)), '.dtrace'], 'w');
    
    % write_signalnames contains the signal names belonging to the group
    % and to be written to both the class and trace files
    write_signalnames = [];
    
    % write_data contains the samples from signals belonging to the group
    % and to be written to the trace file
    write_data = [];
    writefile_flag = 0;
    
    nb_sims = length(simout);
    for j = 1:nb_sims
        if isempty(find(failed_tests == j))
            index = 1;
            %if ~((j == 59)|(j == 60)|(j == 88))
            for k = 1:nb_signals
                %for k = 1:2
                if ( signal_names{k,2} == group_nb(i) )
                    if ( keep_sig(k) )
                        write_data(:,index) = simout{j}(:,k);
                        write_signalnames{index,1} = signal_names{k,1};
                        write_signaltype_num{index,1} = signal_names{k,3};
                        if ( signal_names{k,3} == 0 )
                            write_signaltype{index,1} = 'float';
                        else if ( signal_names{k,3} == 1 || signal_names{k,3} == 2 || signal_names{k,3} == 3 )
                                write_signaltype{index,1} = 'int';
                            else
                                write_signaltype{index,1} = 'float';
                            end
                        end
                        index = index + 1;
                    end
                end
            end
            
            if (~isempty(write_data))
                file_data{j} = write_data;
                writefile_flag = 1;
            else
                writefile_flag = 0;
            end
        end
    end
    
    if ( writefile_flag )
        for l = 1:nb_sims
            if isempty(find(failed_tests == l))
                %if ~((l == 59)|(l == 60)|(l == 88))
                disp(['signals_daikonWR.m: Processing result of test ',num2str(l),' of group ',num2str(i)]);
                [datasam_nb, datasig_nb] = size(file_data{l});
                for m = 1:datasam_nb
                    fprintf(fileID_trace,'myProgramPoint.m():::OBJECT1\n');
                    for n = 1:datasig_nb
                        var_name = string(write_signalnames(n,1));
                        var_value = file_data{l}(m,n);
                        var_signaltype = string(write_signaltype(n,1));
                        var_signaltype_num = cell2mat(write_signaltype_num(n,1));
                        if var_signaltype_num == 0
                            formatspec = '%s\n%f\n1\n';
                        else
                            formatspec = '%s\n%d\n1\n';
                        end
                        
                        fprintf(fileID_trace, formatspec, var_name, var_value);
                        if (l == 1 & m == 1)
                            if (n == 1)
                                fprintf(fileID_class,'DECLARE\n');
                                fprintf(fileID_class,'myProgramPoint.m():::OBJECT1\n');
                                
                            end
                            formatcls = '%s\n%s\n%s\n%d\n';
                            fprintf(fileID_class, formatcls, var_name, var_signaltype, var_signaltype, var_signaltype_num);
                        end
                    end
                    fprintf(fileID_trace,'\n');
                end
                %end
            else
                disp(['signals_daikonWR.m: Failed test ', num2str(l), ' excluded in group ', num2str(i)] );
            end
        end
    end
    fclose(fileID_trace);
    fclose(fileID_class);
end

end