function[ failed_simout ] = extract_failed_simout( combined_simout, failed_test_list )
% FUNCTION: Extracting failed test data from simout
% Created:  28-01-2020
% Author:   Niveditha Manjunath

index = 1;
for i = 1:length(combined_simout)
    if ismember(i, failed_test_list)
        failed_simout{index} = combined_simout{i};
        index = index + 1;
    end
end

end